package io.muic.ooc.Command.Commands;

import io.muic.ooc.Character.Player;
import io.muic.ooc.Command.Command;
import io.muic.ooc.Item.Armoury.Armour;
import io.muic.ooc.Item.Armoury.Weapon;
import io.muic.ooc.Item.Consumable.Consumable;
import io.muic.ooc.Item.Item;

import java.util.Map;

/**
 * Created by tyeon on 2/2/17.
 */
public class UseCommand implements Command {

    String itemName = null;
    public void setArg(String arg) {
        itemName = arg;
    }
    public void apply() {
        Player player = Player.getInstance();
        Map<Item, Integer> inventory = player.getInventory();
        for (Map.Entry<Item, Integer> item : inventory.entrySet()) {
            if (item.getKey().getName().equalsIgnoreCase(itemName)) {
                Item key = item.getKey();
                if (key instanceof Weapon) {
                    player.equipWeapon((Weapon) key);
                    System.out.println("You have equipped " + key.getName());
                }
                else if (key instanceof Armour) {
                    player.equipArmour((Armour) key);
                    System.out.println("You have equipped " + key.getName());
                }
                else if (key instanceof Consumable) {
                    player.adjustHP(((Consumable) key).getHeal());
                    System.out.println("Your HP has been healed to " + player.getHP());
                    player.dropItem(itemName, 1);
                }
                return;
            }
        }
        System.out.println("You don't have that item.");
    }
}
