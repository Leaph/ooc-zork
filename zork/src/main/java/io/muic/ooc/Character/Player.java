package io.muic.ooc.Character;

import io.muic.ooc.GameMap.Room.Room;
import io.muic.ooc.Item.Armoury.Armour;
import io.muic.ooc.Item.Armoury.Weapon;
import io.muic.ooc.Item.Item;
//import io.muic.ooc.Item.LevelKey.LevelKey;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tyeon on 1/30/17.
 */
public class Player extends Character {
    private int INVENTORY_SLOTS = 5;
    private Map<Item, Integer> inventory = new HashMap<Item, Integer>();
    private Room currentRoom = null;
    private boolean playing = true;

    private static Player player = null;
    private Player() {}

    public static Player getInstance() {
        if (null == player) player = new Player();
        return player;
    }

    public void equipWeapon(Weapon weapon) {
        Weapon currentWeapon = getWeapon();
        setWeapon(weapon);
        dropItem(weapon.getName(), 1);
        if (currentWeapon != null) {
            addItem(currentWeapon);
        }
    }
    public void equipArmour(Armour armour) {
        Armour currentArmour = getArmour();
        setArmour(armour);
        dropItem(armour.getName(), 1);
        if (currentArmour != null) addItem(currentArmour);
    }

    public boolean addItem(Item item) {
        if (INVENTORY_SLOTS > 0) {
            if (inventory.containsKey(item)) inventory.put(item, inventory.get(item)+1);
            else inventory.put(item, 1);
            INVENTORY_SLOTS--;
            return true;
        }
        return false;
    }

    public Map<Item, Integer> getInventory() {
        return inventory;
    }

    public boolean dropItem(String itemName, int quantity) {
        for (Map.Entry<Item, Integer> item : inventory.entrySet()) {
            Item key = item.getKey();
            int value = item.getValue();
            if (key.getName().equalsIgnoreCase(itemName)) {
                int newQuantity = value - quantity;
                if (newQuantity > 0) inventory.put(key, newQuantity);
                else inventory.remove(key);
                INVENTORY_SLOTS++;
                return true;
            }
        }
        return false;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }
    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    public void getStatus(){
        super.getStatus();
        System.out.print("Inventory: ");
        for (Map.Entry<Item, Integer> item : inventory.entrySet()) System.out.print(item.getKey().getName()+", ");
        if (inventory.size() < 1) System.out.println("empty");
        System.out.println();
    }

    public boolean isPlaying() {
        return playing;
    }
    public void setPlaying(boolean playing) {
        this.playing = playing;
    }
}
