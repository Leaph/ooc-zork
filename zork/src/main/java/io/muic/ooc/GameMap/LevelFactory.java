package io.muic.ooc.GameMap;

import io.muic.ooc.GameMap.Room.Room;

import java.util.List;

/**
 * Created by tyeon on 2/2/17.
 */
public class LevelFactory {

    private List<Room> levelOne;
    private List<Room> levelTwo;
    private List<Room> levelThree;
    private GameMap gameMap = new GameMap();

    public List<Room> getLevel(LevelNumber levelNumber) {
        if (levelNumber == LevelNumber.ONE) {
            if (levelOne == null) createLevelOne();
            return levelOne;
        }
        else if (levelNumber == LevelNumber.TWO) {
            if (levelTwo == null) createLevelTwo();
            return levelTwo;
        }
        else if (levelNumber == LevelNumber.THREE) {
            if (levelThree == null) createLevelThree();
            return levelThree;
        }
        else return null;
    }

    private void createLevelOne() {
        levelOne = gameMap.createLevelOne();
    }
    private void createLevelTwo() {
        levelTwo = gameMap.createLevelTwo();
    }
    private void createLevelThree() {
        levelThree = gameMap.createLevelThree();
    }
}
