package io.muic.ooc.GameMap.Room;

/**
 * Created by tyeon on 2/1/17.
 */
public enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST
}
