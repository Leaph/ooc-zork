package io.muic.ooc.Item;

/**
 * Created by tyeon on 1/30/17.
 */
public abstract class Item {
    private String name;
    private String description;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
