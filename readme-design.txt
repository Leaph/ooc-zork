Items abstract class as there are two classes: armoury and consumables which have
different uses are separated.

Character abstract class as there are two classes: the player and aliens which are
separated for the same reasons as above.

A Command interface since there are many different Commands that are implemented, so to enforce that each of these commands have the same basic underlying functionality. This also allows me the Adapter/Wrapper/Facade benefits which lets all the different commands to work together which otherwise would not have been due to incompatible interfaces.

I use a Singleton for the player since the scope of the zork game is limited to Singleplayer. This also helps make referencing the single Player in the game since the singleton makes it a lot more accessible from anywhere. Singleton pattern also ensures that all out access the only single instance of the player.

I use the Factory creational pattern for all Level, Command, Item and, Aliens. This was done to abstract away from specific Constructor methods which makes creating new Objects much more convenient and simple. Also enforces DRY principle of coding.

Chain of responsibility for the level creations also as a form of lazy-initialisation since it is unnecessary to create all three levels at the beginning of the game.

Iterator for the Items so that once used or dropped, it is easy to remove using the Iterator interface without any hassle