package io.muic.ooc.Command.Commands;

import io.muic.ooc.Character.Player;
import io.muic.ooc.Command.Command;
import io.muic.ooc.GameMap.Room.Room;

/**
 * Created by tyeon on 2/1/17.
 */
public class InfoCommand implements Command {

    private String type;

    public void setArg(String arg) {
        type = arg;
    }
    public void apply() {
        Player player = Player.getInstance();
        Room room = player.getCurrentRoom();
        if (type.equalsIgnoreCase("room")) room.getInfo();
        else if (type.equalsIgnoreCase("me")) player.getStatus();
        else if (type.equalsIgnoreCase("alien")) {
            if (room.getAlien() != null) room.getAlien().getStatus();
            else System.out.println("There is no alien in this room");
        }
        else System.out.println("Available info on: me, room, alien");
    }
}
