package io.muic.ooc.Command.Commands;

import io.muic.ooc.Character.Player;
import io.muic.ooc.Command.Command;
import io.muic.ooc.GameMap.LevelNumber;
import io.muic.ooc.GameMap.LevelFactory;
import io.muic.ooc.GameMap.Room.Room;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by tyeon on 2/3/17.
 */
public class ClimbCommand implements Command {

    private void levelTwo() {
        System.out.println("You find it strange that you have yet to find a single body..");
        System.out.println("You fear for the worst. There are no scientists who survived.");
        System.out.println("There are no survivors, period.");
        System.out.println("You are the only human left on this colony. You need to leave ASAP.");
        System.out.println("You quickly decide the best course of action is to head up to the upper floors");
        System.out.println("and attempt to rendezvous at the original location you were supposed to bring the survivors");
        try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) {}
    }

    public void setArg(String arg) {}
    public void apply() {
        Player player = Player.getInstance();
        Room room = player.getCurrentRoom();
        if (!room.hasStairway()) {
            System.out.println("There are no stairs in this room");
            return;
        }

        if (room.getAlien() != null && room.getAlien().getHP() > 0) {
            System.out.println("You must fight the alien blocking the stairway first!");
            return;
        }

        LevelNumber levelNumber = room.getLevelNumber();
        if (levelNumber != levelNumber.getLastLevel()) {
            LevelFactory levelFactory = new LevelFactory();
            List<Room> nextLevel = levelFactory.getLevel(levelNumber.next());
            Room newRoom = nextLevel.get(0);
            player.setCurrentRoom(newRoom);
            if (levelNumber.equals(LevelNumber.ONE)) levelTwo();
            System.out.println("You climb up to a new floor..");
            newRoom.getInfo();
        } else {
            System.out.println("You managed to escape. You're finally free!");
            System.exit(0);
        }

    }
}
