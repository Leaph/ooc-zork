**HELP**
info [OBJECT] - to get intel on [me, room, alien]
walk [DIRECTION] - to walk [north, east, south west] of your current room
take [ITEM] - to pick up an [ITEM] in the room
drop [ITEM] - to drop an [ITEM] in your inventory to make room for another item
use [ITEM] - to use or wield an [ITEM] in your inventory
fight - to fight the alien in the room, if any
climb - to climb the stairs, if any in the room
quit - to exit the game
********


You have a total of 8 commands (excluding help).

‘info’ followed by one of the following: ‘me’, ‘room’, ‘alien’ shows information based on the 2nd argument.
‘info alien’ will show the HP, weapon and armour equipped by the alien.
‘info me’ will show the same but for the player and their current inventory.
‘info room’ will show items in the room, the exits and the direction they are at, and if there are any aliens or stairs which lead to the next level.

‘walk’ followed by a DIRECTION: ‘north’, ‘east’, ‘south’, ‘west’ will take the player to a room which is connected from the current room. Depending on the room, some options may not be available

‘take’ followed by an ITEM will add the item to the player’s inventory if and only if that item exists in the room and there is sufficient space in the player’s inventory.

‘drop’ followed by an ITEM will drop the item from the player’s inventory into the abyss, if and only if the item exists in the player’s inventory.

‘use’ followed by an ITEM will either consume a CONSUMABLE or wield a WEAPON or ARMOUR. If the player is already wielding a weapon/armour the new one will be worn and the old one will be placed in their inventory.

‘fight’ will only be triggered when an alien is present in the room. This will take the player into a turn-by-turn automatic fighting phase which will last until one of the participant dies.

‘climb’ will only be triggered when there is a stairway in the room. This will take the player into a new level.

‘quit’ can be triggered at any point in the game. This will pre-maturely end the game.


The WINNING CONDITION is for the player to climb up 3 levels and kill the alien Queen.
The player will LOSE if they die from loss of all their HP.

There are two types of CHARACTERS: the Player themselves and ALIENS that are randomly scattered around each level. Each alien have different HP and base damage and can have weapon or armour equipped. The player is the same, except that they also have an inventory that they can use to collect items for later use.

WEAPONS and ARMOUR both have a durability and a multiplier. The durability shows how long the item will last for. The durability is decremented after each turn of fighting (-1 armour durability for being hit, -1 weapon durability for hitting). The multiplier of a weapon is multiplied on the character’s base damage.
ie. base_damage * (1+weapon.multiplier) = weapon_damage
but this will then be decreased if the character receiving the damage has some armour
ie. weapon_damage * (1-armour.multiplier) = actual_damage_received


CONSUMABLES only effect HP, and when used will increase the player’s HP by a set amount.

The FIGHTING is automatically simulated when the command is given. Using the mechanics described above, the player will attack the alien first, followed by the alien attacking the player. This will continue until either the player or alien is dead. If the player dies, the game is over. If the alien dies, the player is brought back to the room with full control and may proceed to play.