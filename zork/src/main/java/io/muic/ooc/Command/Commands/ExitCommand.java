package io.muic.ooc.Command.Commands;

import io.muic.ooc.Command.Command;

/**
 * Created by tyeon on 2/1/17.
 */
public class ExitCommand implements Command {
    public void setArg(String arg) {}
    public void apply() {
        System.exit(0);
    }
}
