package io.muic.ooc.Item.Armoury;

import io.muic.ooc.Item.Item;

/**
 * Created by tyeon on 1/31/17.
 */
public abstract class Armoury extends Item {
    private int durability;
    private double multiplier;

    public int getDurability() {
        return durability;
    }
    public void setDurability(int durability) {
        this.durability = durability;
    }
    public void decrementDurability() {
        durability--;
    }

    public double getMultiplier() {
        return multiplier;
    }
    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }
}
