#STORY AND OBJECTIVE#

The year is 2084.

You are about to land on Adraylea, a colonized planet that was recently run
over by invasive aliens. You have been deployed to
search for survivors, particularly the scientists that have been leading
experiments that was commisioned by the federation. Chances are slim that any
of them are alive, which makes you wonder why you were tasked with a mission
that was pratically suicidal. But then again, albeit questionable at times,
the federation wouldn't be what it is without its unorthodox leadership.
Before you land, you take another look a the mission:

--------------------

You have been selected to lead a group of marines to the planet of Adraylea.
The planet has been taken over by an invasive alien species.

1. You are to search the area for scientists and other personnel and
2. bring them back to secure location for extraction.
3. If need be, you are authorised to destroy the colony to prevent the aliens
   from spreading.

There is limited intel on the extra-terrestrials, so be careful.
Good luck.

--------------------

You land.
