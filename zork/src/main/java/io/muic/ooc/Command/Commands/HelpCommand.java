package io.muic.ooc.Command.Commands;

import io.muic.ooc.Command.Command;

/**
 * Created by tyeon on 2/2/17.
 */
public class HelpCommand implements Command {

    public void setArg(String arg) {}
    public void apply() {
        System.out.println("**HELP**");
        System.out.println("info [OBJECT] - to get intel on [me, room, alien]");
        System.out.println("walk [DIRECTION] - to walk [north, east, south west] of your current room");
        System.out.println("take [ITEM] - to pick up an [ITEM] in the room");
        System.out.println("drop [ITEM] - to drop an [ITEM] in your inventory to make room for another item");
        System.out.println("use [ITEM] - to use or wield an [ITEM] in your inventory");
        System.out.println("fight - to fight the alien in the room, if any");
        System.out.println("climb - to climb the stairs, if any in the room");
        System.out.println("help - to show this menu");
        System.out.println("quit - to exit the game");
        System.out.println("********");
    }
}
