package io.muic.ooc.Command;

/**
 * Created by tyeon on 2/1/17.
 */
public interface Command {
    void apply();
    void setArg(String arg);
}