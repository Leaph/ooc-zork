package io.muic.ooc.Character;

import io.muic.ooc.GameMap.Level;
import io.muic.ooc.GameMap.LevelNumber;
import io.muic.ooc.GameMap.Room.Room;
import io.muic.ooc.Item.Armoury.Armour;
import io.muic.ooc.Item.Armoury.Weapon;
import io.muic.ooc.Item.Consumable.Consumable;
import io.muic.ooc.Item.Item;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by tyeon on 2/3/17.
 */
public class PlayerTest {

    @Test
    public void isPlayer() throws Exception {
        Player player = Player.getInstance();
        assert player instanceof Player;
    }

    @Test
    public void isSingleton() throws Exception {
        Player player = Player.getInstance();
        Player bossy = Player.getInstance();

        assert bossy.equals(player);
    }

    @Test
    public void getInventory() throws Exception {
        Map<Item, Integer> inventory = new HashMap<Item, Integer>();
        assert Player.getInstance().getInventory().equals(inventory);
    }

    @Test
    public void equipWeapon() throws Exception {
        Player player = Player.getInstance();
        Weapon weapon = new Weapon();
        player.equipWeapon(weapon);
        assert player.getWeapon().equals(weapon);
    }

    @Test
    public void equipArmour() throws Exception {
        Player player = Player.getInstance();
        Armour armour = new Armour();
        player.equipArmour(armour);
        assert player.getArmour().equals(armour);
    }

    @Test
    public void addItem() throws Exception {
        Player player = Player.getInstance();
        Consumable item = new Consumable();
        assert player.addItem(item) && player.getInventory().get(item) == 1;
    }

    @Test
    public void fullItem() throws Exception {
        Player player = Player.getInstance();
        Consumable item = new Consumable();
        player.addItem(item);
        player.addItem(item);
        player.addItem(item);
        player.addItem(item);
        player.addItem(item);
        assertFalse(player.addItem(item));
    }

    @Test
    public void dropItemThatInventoryDoesNotContain() throws Exception {
        Player player = Player.getInstance();
        assertFalse(player.dropItem("bla", 1));
    }

    @Test
    public void dropItemThatInvetoryContainsMany() throws Exception {
        Player player = Player.getInstance();
        Consumable consumable = new Consumable();
        consumable.setName("heal");
        player.addItem(consumable);
        player.addItem(consumable);
        assert player.getInventory().containsKey(consumable);
    }

    @Test
    public void getCurrentRoom() throws Exception {
        Player player = Player.getInstance();
        Room room = new Room();
        player.setCurrentRoom(room);
        assert player.getCurrentRoom() instanceof Room;
    }

    @Test
    public void setCurrentRoom() throws Exception {
        Player player = Player.getInstance();
        Room room = new Room();
        room.setLevelNumber(LevelNumber.THREE);
        player.setCurrentRoom(room);
        assert player.getCurrentRoom().getLevelNumber().equals(LevelNumber.THREE);
    }

    @Test
    public void isPlaying() throws Exception {
        Player player = Player.getInstance();
        player.setPlaying(true);
        assert player.isPlaying();
    }

    @Test
    public void setPlaying() throws Exception {
        Player player = Player.getInstance();
        player.setPlaying(false);
        assertFalse( player.isPlaying());
    }

}