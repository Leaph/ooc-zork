package io.muic.ooc.Command.Commands;

import io.muic.ooc.Character.Player;
import io.muic.ooc.Command.Command;

/**
 * Created by tyeon on 2/3/17.
 */
public class DropCommand implements Command {

    String itemName = null;

    public void setArg(String arg) {
        itemName = arg;
    }
    public void apply() {
        Player player = Player.getInstance();
        if (player.dropItem(itemName, 1)) System.out.println(itemName+" was dropped.");
        else System.out.println("You don't have any of those in your inventory");
    }
}
