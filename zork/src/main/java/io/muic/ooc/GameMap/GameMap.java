package io.muic.ooc.GameMap;

import io.muic.ooc.Character.AlienFactory;
import io.muic.ooc.GameMap.Room.Room;
import io.muic.ooc.Item.ItemFactory;

import java.util.List;
import java.util.Random;

/**
 * Created by tyeon on 1/31/17.
 */
public class GameMap {

    private final static Random random = new Random();
    private final static AlienFactory alienFactory = new AlienFactory();
    private final static ItemFactory itemFactory = new ItemFactory();

    public List<Room> createLevelOne() {
        Level level = new Level();
        level.generateVanillaLevel(LevelNumber.ONE);
        //exit
        level.setStairWay(random.nextInt(7)+2);
        //aliens
        level.setAlien(random.nextInt(8)+1, alienFactory.createAlien("default"));
        level.setAlien(8, alienFactory.createAlien("default"));
        //items
        level.addItem(1, itemFactory.createConsumable("salve", 100));
        level.addItem(random.nextInt(7)+2, itemFactory.createConsumable("aspirin", 50));
        level.addItem(random.nextInt(7)+2, itemFactory.createConsumable("chocolate", 15));
        level.addItem(random.nextInt(7)+2, itemFactory.createArmoury("armour", "cuirass",0.2, 3));
        return level.getRooms();
    }

    public List<Room> createLevelTwo() {
        Level level = new Level();
        level.generateVanillaLevel(LevelNumber.TWO);
        //exit
        level.setStairWay(random.nextInt(7)+2);
        //aliens
        level.setAlien(0, alienFactory.createAlien("strong"));
        level.setAlien(8, alienFactory.createAlien("strong"));
        level.setAlien(random.nextInt(8)+1, alienFactory.createAlien("strong"));
        //items
        level.addItem(0, itemFactory.createArmoury("weapon", "lightsaber", 0.5, 7));
        level.addItem(random.nextInt(7)+2, itemFactory.createConsumable("salve", 50));
        level.addItem(random.nextInt(7)+2, itemFactory.createConsumable("salve", 50));
        level.addItem(random.nextInt(7)+2, itemFactory.createConsumable("salve", 50));
        return level.getRooms();
    }

    public List<Room> createLevelThree() {
        Level level = new Level();
        level.generateVanillaLevel(LevelNumber.THREE);
        //exit
        level.setStairWay(5);
        //aliens
        level.setAlien(0, alienFactory.createAlien("strong"));
        level.setAlien(5, alienFactory.createAlien("boss"));
        level.setAlien(8, alienFactory.createAlien("strong"));
        level.setAlien(random.nextInt(8)+1, alienFactory.createAlien("strong"));
        //items
        level.addItem(9, itemFactory.createArmoury("armour", "ironsuit", 0.6, 10));
        level.addItem(random.nextInt(7)+2, itemFactory.createArmoury("weapon", "blaster", .5, 50));
        level.addItem(random.nextInt(7)+2, itemFactory.createConsumable("salve", 50));
        level.addItem(random.nextInt(7)+2, itemFactory.createConsumable("salve", 50));
        return level.getRooms();
    }
}