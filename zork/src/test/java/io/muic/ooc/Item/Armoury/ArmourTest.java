package io.muic.ooc.Item.Armoury;

import io.muic.ooc.Item.Item;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by tyeon on 2/3/17.
 */
public class ArmourTest {

    @Test
    public void armour() throws Exception {
        Armour armour = new Armour();
        String name = "weapon";
        armour.setName(name);
        assert armour instanceof Item && armour instanceof Armour && armour.getName().equals(name);
    }

    @Test
    public void getAndSetDurability() throws Exception {
        Armour armour = new Armour();
        armour.setDurability(50);
        assert armour.getDurability() == 50;
    }

    @Test
    public void getAndSetMultiplier() throws Exception {
        Armour armour = new Armour();
        armour.setMultiplier(0.5);
        assert armour.getMultiplier() == 0.5;
    }

}