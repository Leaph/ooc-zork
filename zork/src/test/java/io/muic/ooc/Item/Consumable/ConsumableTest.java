package io.muic.ooc.Item.Consumable;

import io.muic.ooc.Item.Item;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by tyeon on 2/3/17.
 */
public class ConsumableTest {

    @Test
    public void consumable() throws Exception {
        Consumable consumable = new Consumable();
        String name = "heal";
        consumable.setName(name);
        assert consumable instanceof Item && consumable instanceof Consumable && consumable.getName().equals(name);
    }
    @Test
    public void setAndGetHeal() throws Exception {
        Consumable consumable = new Consumable();
        consumable.setHeal(50);
        assert consumable.getHeal() == 50;
    }

}