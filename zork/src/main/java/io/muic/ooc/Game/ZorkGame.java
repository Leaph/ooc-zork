package io.muic.ooc.Game;

import io.muic.ooc.Character.Player;
import io.muic.ooc.Command.Command;
import io.muic.ooc.Command.CommandFactory;
import io.muic.ooc.GameMap.LevelNumber;
import io.muic.ooc.GameMap.LevelFactory;
import io.muic.ooc.GameMap.Room.Room;

import java.util.List;
import java.util.Scanner;

/**
 * Created by tyeon on 2/1/17.
 */
public class ZorkGame {
    private CommandFactory commandFactory = new CommandFactory();

    private void preGameText() {
        System.out.println("The year is 2084.");
        System.out.println("You are about to land on Adraylea, a colonized planet that was recently run");
        System.out.println("over by invasive aliens. You have been deployed to");
        System.out.println("search for survivors, particularly the scientists that have been leading");
        System.out.println("experiments that was commisioned by the federation. Chances are slim that any");
        System.out.println("of them are alive, which makes you wonder why you were tasked with a mission");
        System.out.println("that was pratically suicidal. But then again, albeit questionable at times,");
        System.out.println("the federation wouldn't be what it is without its unorthodox leadership.");
        System.out.println("Before you land, you play another look a the mission:\n");
        System.out.println("--------------------\n");
        System.out.println("You have been selected to lead a group of marines to the planet of Adraylea.");
        System.out.println("The planet has been taken over by an invasive alien species.\n");
        System.out.println("1. You are to search the area for scientists and other personnel and");
        System.out.println("2. bring them back to secure location for extraction.");
        System.out.println("3. If need be, you are authorised to destroy the colony to prevent the aliens");
        System.out.println("from spreading.\n");
        System.out.println("There is limited intel on the extra-terrestrials, so be careful.");
        System.out.println("Good luck.\n");
        System.out.println("--------------------\n\n");
        System.out.println("You land.");
    }

    private void postGameText() {
        System.out.println("You climb out, and see the medevac hovering over, waiting for you.");
        System.out.println("You get on board, exhausted.");
        System.out.println("With the Queen dead, you can't help but feel proud for doing it alone");
        System.out.println("You won!");
    }

    public void play() {
        LevelFactory levelFactory = new LevelFactory();
        List<Room> levelOne = levelFactory.getLevel(LevelNumber.ONE);
        Room start = levelOne.get(0);

        Player player = Player.getInstance();
        player.setHP(300);
        player.setBaseDamage(50);
        player.setCurrentRoom(start);

        preGameText();
        start.getInfo();

        Scanner scanner = new Scanner(System.in);
        while (player.isPlaying()) {
            System.out.print("You: ");
            String[] commandLine = scanner.nextLine().split("\\s+");
            Command command = commandFactory.getCommand(commandLine);
            if (null != command) command.apply();
        }

        postGameText();
    }
}
