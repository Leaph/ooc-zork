package io.muic.ooc.GameMap;

/**
 * Created by tyeon on 2/3/17.
 */
public enum LevelNumber {
    ONE, TWO, THREE;

    private int number;
    private LevelNumber next;

    static {
        ONE.next = TWO;
        TWO.next = THREE;
        THREE.next = null;
    }

    public LevelNumber next() {return next; };
    public LevelNumber getLastLevel() { return THREE; };
}
