package io.muic.ooc.GameMap;

import io.muic.ooc.Character.Alien;
import io.muic.ooc.GameMap.Room.Direction;
import io.muic.ooc.GameMap.Room.Room;
import io.muic.ooc.Item.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tyeon on 2/3/17.
 */
public class Level {
    private List<Room> rooms = new ArrayList<>();

    public List<Room> generateVanillaLevel(LevelNumber levelNumber) {
        for (int i = 0; i < 9; i++) {
            rooms.add(new Room());
        }

        for (int i = 0; i < 9; i++) {
            Room room = rooms.get(i);
            if (i-3 >= 0) room.setExit(Direction.NORTH, rooms.get(i-3));
            if (i+1 < 9) room.setExit(Direction.EAST, rooms.get(i+1));
            if (i+3 < 9) room.setExit(Direction.SOUTH, rooms.get(i+3));
            if (i-1 >= 0) room.setExit(Direction.WEST, rooms.get(i-1));
            room.setLevelNumber(levelNumber);
        }
        return rooms;
    }

    private boolean checkRoomIndex(int roomNumber) {
        return roomNumber >= 0 && roomNumber < rooms.size();
    }

    public void setStairWay(int roomNumber) {
        if (checkRoomIndex(roomNumber)) rooms.get(roomNumber).buildStairway(true);
    }

    public void setAlien(int roomNumber, Alien alien) {
        if (checkRoomIndex(roomNumber)) rooms.get(roomNumber).setAlien(alien);
    }

    public void addItem(int roomNumber, Item item) {
        if (checkRoomIndex(roomNumber)) rooms.get(roomNumber).addItem(item);
    }

    public List<Room> getRooms() {
        return rooms;
    }
}
