package io.muic.ooc.Item;

import io.muic.ooc.Item.Armoury.Armour;
import io.muic.ooc.Item.Armoury.Armoury;
import io.muic.ooc.Item.Consumable.Consumable;
import io.muic.ooc.Item.Armoury.Weapon;

import java.util.*;

/**
 * Created by tyeon on 1/31/17.
 */
public class ItemFactory {

    private static Map<String, Class<? extends Armoury>> armouryClasses =
            new HashMap<String, Class<? extends Armoury>>() {
                {
                    put("armour", Armour.class);
                    put("weapon", Weapon.class);
                }
            };

    public Armoury createArmoury(String armouryType, String name, double multiplier, int durability) {
        Class<? extends Armoury> armouryClass = armouryClasses.get(armouryType);
        Armoury armoury = null;
        try {
            armoury = armouryClass.newInstance();
            armoury.setName(name);
            armoury.setMultiplier(multiplier);
            armoury.setDurability(durability);
        } catch (InstantiationException | IllegalAccessException ex) {
        } catch (NullPointerException npe) {
            System.out.println("Not found item type: " + armouryType);
        }
        return armoury;
    }

    public Consumable createConsumable(String name, int heal) {
        Consumable consumable = new Consumable();
        consumable.setName(name);
        consumable.setHeal(heal);
        return consumable;
    }

}
