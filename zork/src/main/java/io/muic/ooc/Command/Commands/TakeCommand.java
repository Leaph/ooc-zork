package io.muic.ooc.Command.Commands;

import io.muic.ooc.Character.Player;
import io.muic.ooc.Command.Command;
import io.muic.ooc.GameMap.Room.Room;
import io.muic.ooc.Item.Item;

import java.util.Iterator;

/**
 * Created by tyeon on 2/2/17.
 */
public class TakeCommand implements Command {

    private String itemName;

    public void setArg(String arg) {
        itemName = arg;
    }
    public void apply() {
        Player player = Player.getInstance();
        Room room = player.getCurrentRoom();

        if (room.getAlien() != null && room.getAlien().getHP() > 0) {
            System.out.println("You must kill the alien in the room before you can take anything!");
            return;
        }

        for (Iterator<Item> iterator = room.getItems().iterator(); iterator.hasNext();) {
            Item item = iterator.next();
            if (item.getName().equalsIgnoreCase(itemName)) {
                if (player.addItem(item)) {
                    System.out.println("You can find the " + itemName + " in your inventory.");
                    iterator.remove();
                } else System.out.println("Your inventory is full! Drop or use something to make room.");
                return;
            }
        }
        System.out.println("There is no "+itemName);
    }
}
