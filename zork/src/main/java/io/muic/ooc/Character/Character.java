package io.muic.ooc.Character;

import io.muic.ooc.Item.Armoury.Armour;
import io.muic.ooc.Item.Consumable.Consumable;
import io.muic.ooc.Item.Armoury.Weapon;

/**
 * Created by tyeon on 1/30/17.
 */
public abstract class Character {
    private int HP;
    private int baseDamage;
    private Armour armour;
    private Weapon weapon;

    public int getBaseDamage() {
        return baseDamage;
    }
    public void setBaseDamage(int baseDamage) {
        this.baseDamage = baseDamage;
    }

    public int getHP() {
        return HP;
    }
    public void setHP(int HP) {
        this.HP = HP;
    }
    public void adjustHP(int add) {
        HP += add;
    }

    public Weapon getWeapon() {
        return weapon;
    }
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Armour getArmour() {
        return armour;
    }
    public void setArmour(Armour armour) {
        this.armour = armour;
    }

    public void getStatus() {
        System.out.println("Current HP: " + HP);
        if (weapon != null) System.out.println("Weapon equipped: "+weapon.getName() + ", multiplier: "+weapon.getMultiplier() +", durability: "+weapon.getDurability());
        else System.out.println("No weapon equipped");
        if (armour != null)System.out.println("Armour equipped: "+armour.getName() + ", multiplier: "+armour.getMultiplier() +", durability: "+armour.getDurability());
        else System.out.println("No armour equipped");
    }
}
