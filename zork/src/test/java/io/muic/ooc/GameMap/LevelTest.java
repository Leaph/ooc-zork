package io.muic.ooc.GameMap;

import io.muic.ooc.Character.Alien;
import io.muic.ooc.Item.Consumable.Consumable;
import io.muic.ooc.Item.Item;
import io.muic.ooc.Item.ItemFactory;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by tyeon on 2/3/17.
 */
public class LevelTest {

    @Test
    public void setStairWay() throws Exception {
        Level level = new Level();
        level.generateVanillaLevel(LevelNumber.THREE);
        level.setStairWay(3);

        assert level.getRooms().get(3).hasStairway();
    }

    @Test
    public void setAlien() throws Exception {
        Level level = new Level();
        level.generateVanillaLevel(LevelNumber.THREE);
        Alien alien = new Alien();
        alien.setHP(300);
        level.setAlien(3, alien);

        assert level.getRooms().get(3).getAlien().equals(alien);
    }

    @Test
    public void addItem() throws Exception {
        Level level = new Level();
        level.generateVanillaLevel(LevelNumber.THREE);
        ItemFactory itemFactory = new ItemFactory();
        Item item = itemFactory.createConsumable("heal", 50);
        level.addItem(3, item);

        assert level.getRooms().get(3).getItems().contains(item);
    }

}