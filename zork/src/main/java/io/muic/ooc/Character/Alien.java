package io.muic.ooc.Character;

/**
 * Created by tyeon on 1/30/17.
 */
public class Alien extends Character {
    private boolean boss = false;

    public boolean isBoss() {
        return boss;
    }
    public void setBoss(boolean boss) {
        this.boss = boss;
    }
}
