package io.muic.ooc.Command.Commands;

import io.muic.ooc.Character.Player;
import io.muic.ooc.Command.Command;
import io.muic.ooc.GameMap.Room.Direction;
import io.muic.ooc.GameMap.Room.Room;

import java.util.Map;

/**
 * Created by tyeon on 2/1/17.
 */
public class WalkCommand implements Command {
    private String destination;

    public void setArg(String arg) {
        destination = arg;
    }
    public void apply() {
        Player player = Player.getInstance();
        Room room = player.getCurrentRoom();
        Map<Direction, Room> exitMap = room.getExitMap();
        try {
            System.out.println("You walk "+destination+"..");
            Direction direction = Direction.valueOf(destination.toUpperCase());
            Room newRoom = exitMap.get(direction);
            newRoom.getInfo();
            player.setCurrentRoom(newRoom);
        } catch (Exception e) {
            System.out.println("You can not go that way.");
            System.out.print("Unknown direction. Available directions: ");
            for (Map.Entry<Direction, Room> exit : exitMap.entrySet()) System.out.print(exit.getKey()+" ");
            System.out.println();
        }
    }
}
