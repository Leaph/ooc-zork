package io.muic.ooc.GameMap.Room;

import io.muic.ooc.Character.Alien;
import io.muic.ooc.GameMap.LevelNumber;
import io.muic.ooc.Item.Consumable.Consumable;
import io.muic.ooc.Item.Item;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by tyeon on 2/3/17.
 */
public class RoomTest {

    @Test
    public void room() throws Exception {
        Room room = new Room();
        assert room instanceof Room;
    }

    @Test
    public void getItems() throws Exception {
        Room room = new Room();
        List<Item> items = new ArrayList<Item>();
        assert room.getItems().equals(items);
    }

    @Test
    public void addItem() throws Exception {
        Room room = new Room();
        Consumable item = new Consumable();
        item.setName("item");
        item.setHeal(50);
        room.addItem(item);

        List<Item> items = new ArrayList<Item>();
        items.add(item);
        assert room.getItems().equals(items);
    }

    @Test
    public void getAlien() throws Exception {
        Room room = new Room();
        Alien alien = new Alien();
        alien.setHP(50);
        room.setAlien(alien);

        assert room.getAlien().equals(alien);
    }

    @Test
    public void getExitMap() throws Exception {
        Room room = new Room();
        Map<Direction, Room> exitMap = new HashMap<Direction, Room>();
        assert room.getExitMap().equals(exitMap);
    }

    @Test
    public void hasStairway() throws Exception {
        Room room = new Room();
        assertFalse(room.hasStairway());
    }

    @Test
    public void getLevelNumber() throws Exception {
        Room room = new Room();
        LevelNumber levelNumber = LevelNumber.THREE;
        room.setLevelNumber(levelNumber);

        assert room.getLevelNumber().equals(LevelNumber.THREE);
    }

}