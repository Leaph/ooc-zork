.DS* - is a file that stores custom attributes specific to a machine, so this will always conflict and it is unnecessary to the project
/.idea - since this folder contains the project settings, we exclude so that the zork application is not IDE specific
*.iml - iml are module files specific to the IDE being used, and for similar reasons to above, will be excluded
target - target folder contains the compiled project which is unneeded as we have the source code
