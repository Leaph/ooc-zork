package io.muic.ooc.Command.Commands;

import io.muic.ooc.Character.Alien;
import io.muic.ooc.Character.Character;
import io.muic.ooc.Character.Player;
import io.muic.ooc.Command.Command;
import io.muic.ooc.GameMap.Room.Room;
import io.muic.ooc.Item.Armoury.Armour;
import io.muic.ooc.Item.Armoury.Armoury;
import io.muic.ooc.Item.Armoury.Weapon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by tyeon on 2/3/17.
 */
public class FightCommand implements Command {

    private Random random = new Random();
    public String processAttackMessage() {
        int i = random.nextInt(attackResponse.size());
        return attackResponse.get(i);
    }

    private List<String> attackResponse = new ArrayList<String>() {
        {
            add("hit");
            add("strike");
            add("attack");
            add("charge");
        }
    };

    private double criticalChance() {
        return random.nextInt(4)/10.0;
    }

    private void checkDurability(Character character, Armoury armoury) {
        if (armoury instanceof Weapon) {
            if (armoury.getDurability() <= 0) character.setWeapon(null);
        } else if (armoury instanceof  Armour) {
            if (armoury.getDurability() <= 0) character.setArmour(null);
        }
    }

    private int getTurnDamage(Character attacker, Character defender) {
        float attackerDamage = attacker.getBaseDamage();
        Weapon attackerWeapon = attacker.getWeapon();
        if (attackerWeapon != null) {
            attackerDamage *= 1+attackerWeapon.getMultiplier();
            attackerWeapon.decrementDurability();
            checkDurability(attacker, attackerWeapon);
        }
        Armour defenderArmour = defender.getArmour();
        if (defenderArmour != null) {
            attackerDamage *= 1-defenderArmour.getMultiplier();
            defenderArmour.decrementDurability();
            checkDurability(defender, defenderArmour);
        }
        return (int) Math.round(attackerDamage*(1+criticalChance()));
    }

    public void setArg(String arg) {
    }
    public void apply() {
        Player player = Player.getInstance();
        Room room = player.getCurrentRoom();
        Alien alien = room.getAlien();
        if (alien == null || alien.getHP() <= 0) {
            System.out.println("There is nothing to fight!");
            return;
        }

        while (player.getHP() > 0 && alien.getHP() > 0) {
            int playerDamage = getTurnDamage(player, alien);
            System.out.println("You "+processAttackMessage()+" the alien for "+playerDamage+"!");
            alien.adjustHP(-playerDamage);
            if (alien.getHP() <= 0) break;
            System.out.println("Alien hp is now "+alien.getHP());
            int alienDamage = getTurnDamage(alien, player);
            System.out.println("The alien "+processAttackMessage()+" you for "+alienDamage+"!");
            player.adjustHP(-alienDamage);
            System.out.println("Your hp is now "+player.getHP());
            try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) {}
        }

        if (player.getHP() > 0) {
            System.out.println("You killed the alien!");
            room.setAlien(null);
        }
        else {
            System.out.println("You died.\nGame Over.");
            System.exit(0);
        }
    }
}
