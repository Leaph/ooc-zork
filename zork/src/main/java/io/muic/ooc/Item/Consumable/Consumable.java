package io.muic.ooc.Item.Consumable;

import io.muic.ooc.Item.Item;

/**
 * Created by tyeon on 1/31/17.
 */
public class Consumable extends Item {
    private int heal;

    public int getHeal() {
        return heal;
    }
    public void setHeal(int heal) {
        this.heal = heal;
    }
}
