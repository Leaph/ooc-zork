package io.muic.ooc.Character;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by tyeon on 2/3/17.
 */
public class AlienTest {
    @Test
    public void isAlien() throws Exception {
        Alien alien = new Alien();
        assert alien instanceof Character && alien instanceof Alien;
    }

    @Test
    public void isBoss() throws Exception {
        Alien alien = new Alien();
        assert alien.isBoss() == false;
    }

    @Test
    public void setBoss() throws Exception {
        Alien alien = new Alien();
        alien.setBoss(true);
        assert alien.isBoss() == true;
    }

}