package io.muic.ooc.Command;

import io.muic.ooc.Command.Commands.*;
import org.junit.Test;

/**
 * Created by tyeon on 2/1/17.
 */
public class CommandFactoryTest {

    CommandFactory commandFactory = new CommandFactory();

    @Test
    public void getExitCommand() throws Exception {
        String[] args = new String[1];
        args[0] = "quit";
        assert commandFactory.getCommand(args) instanceof ExitCommand;
    }

    @Test
    public void getWalkCommand() throws Exception {
        String[] args = new String[2];
        args[0] = "walk";
        args[1] = "south";
        assert commandFactory.getCommand(args) instanceof WalkCommand;
    }

    @Test
    public void getInfoCommand() throws Exception {
        String[] args = new String[2];
        args[0] = "info";
        args[1] = "room";
        assert commandFactory.getCommand(args) instanceof InfoCommand;
    }

    @Test
    public void getHelpCommand() throws Exception {
        String[] args = new String[1];
        args[0] = "help";
        assert commandFactory.getCommand(args) instanceof HelpCommand;
    }

    @Test
    public void getTakeCommand() throws Exception {
        String[] args = new String[2];
        args[0] = "take";
        args[1] = "stimpack";
        assert commandFactory.getCommand(args) instanceof TakeCommand;
    }

    @Test
    public void getDropCommand() throws Exception {
        String[] args = new String[2];
        args[0] = "drop";
        args[1] = "stimpack";
        assert commandFactory.getCommand(args) instanceof DropCommand;
    }

    @Test
    public void getUseCommand() throws Exception {
        String[] args = new String[2];
        args[0] = "use";
        args[1] = "stimpack";
        assert commandFactory.getCommand(args) instanceof UseCommand;
    }

    @Test
    public void getFightCommand() throws Exception {
        String[] args = new String[1];
        args[0] = "fight";
        assert commandFactory.getCommand(args) instanceof FightCommand;
    }

    @Test
    public void getClimbCommand() throws Exception {
        String[] args = new String[1];
        args[0] = "climb";
        assert commandFactory.getCommand(args) instanceof ClimbCommand;
    }
}