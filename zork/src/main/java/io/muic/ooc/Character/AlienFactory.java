package io.muic.ooc.Character;

import io.muic.ooc.Character.Alien;

/**
 * Created by tyeon on 1/31/17.
 */
public class AlienFactory {

    public Alien createAlien(String alienType){
        Alien alien = new Alien();
        alien.setHP(100);
        alien.setBaseDamage(20);
        if (alienType.equals("strong")) {
            alien.setHP(200);
            alien.setBaseDamage(40);
        } else if (alienType.equals("boss")) {
            alien.setHP(500);
            alien.setBaseDamage(80);
            alien.setBoss(true);
        }
        return alien;
    }
}
