package io.muic.ooc.Item;

import io.muic.ooc.Item.ItemFactory;
import io.muic.ooc.Item.Armoury.Armour;
import io.muic.ooc.Item.Armoury.Armoury;
import io.muic.ooc.Item.Consumable.Consumable;
import io.muic.ooc.Item.Armoury.Weapon;
import org.junit.Test;

/**
 * Created by tyeon on 1/31/17.
 */
public class ItemFactoryTest {

    @Test
    public void createWeapon() throws Exception {
        ItemFactory itemFactory = new ItemFactory();
        String name = "lightsaber";
        double multiplier = 2;
        int durability = 10;
        Armoury lightsaber = itemFactory.createArmoury("weapon", name, multiplier, durability);

        assert lightsaber instanceof Weapon && lightsaber.getDurability() == durability &&
                lightsaber.getMultiplier() == multiplier && lightsaber.getName().equals(name);
    }

    @Test
    public void createArmour() throws Exception {
        ItemFactory itemFactory = new ItemFactory();
        String name = "psionic armour";
        double multiplier = 0.2;
        int durability = 5;
        Armoury psionicArmour = itemFactory.createArmoury("armour", name, multiplier, durability);

        assert psionicArmour instanceof Armour && psionicArmour.getDurability() == durability &&
                psionicArmour.getMultiplier() == multiplier && psionicArmour.getName().equals(name);
    }

    @Test
    public void createConsumable() throws Exception {
        ItemFactory itemFactory = new ItemFactory();
        String name = "stimpack";
        int heal = 30;
        Consumable healthPack = itemFactory.createConsumable(name, heal);

        assert healthPack instanceof Consumable && healthPack.getHeal() == 30 && healthPack.getName().equals(name);
    }

}