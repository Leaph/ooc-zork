package io.muic.ooc.GameMap.Room;

import io.muic.ooc.Character.Alien;
import io.muic.ooc.GameMap.LevelNumber;
import io.muic.ooc.Item.Item;

import java.lang.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tyeon on 1/30/17.
 */
public class Room {
    private final static int MAX_ITEMS = 3;
    private List<Item> items = new ArrayList<Item>();
    private Alien alien = null;
    private Map<Direction, Room> exitMap = new HashMap<Direction, Room>();
    private LevelNumber levelNumber;
    private boolean stairway = false;

    public List<Item> getItems() {
        return items;
    }

    public boolean addItem(Item item) {
        if (items.size() < MAX_ITEMS) {
            items.add(item);
            return true;
        }
        return false;
    }

    public void getInfo() {
        System.out.print("You scan the room and you find.. ");
        for (Item item : items) System.out.print(item.getName()+", ");
        if (items.size() < 1) System.out.print("nothing");
        System.out.println();
        for (Map.Entry<Direction, Room> exit : exitMap.entrySet()) {
            System.out.println("There is an exit to the " + exit.getKey());
        }
        if (alien != null && alien.getHP() > 0) {
            System.out.println("There is an ALIEN in this room!");
            if (alien.isBoss()) System.out.println((char)27 +"[1mIt's the Queen" + (char)27+"[0m");
            System.out.println("You must fight!");
        }
        if (stairway) System.out.println("There are some stairs at the back.");
    }

    public Alien getAlien() {
        return alien;
    }
    public void setAlien(Alien alien) {
        if (this.alien == null) this.alien = alien;
    }

    public Map<Direction, Room> getExitMap() {
        return exitMap;
    }
    public void setExit(Direction direction, Room exitingRoom) {
             if (direction == Direction.NORTH) exitMap.put(Direction.NORTH, exitingRoom);
        else if (direction == Direction.EAST)  exitMap.put(Direction.EAST, exitingRoom);
        else if (direction == Direction.SOUTH) exitMap.put(direction.SOUTH, exitingRoom);
        else if (direction == Direction.WEST)  exitMap.put(Direction.WEST, exitingRoom);
    }

    public boolean hasStairway() {
        return stairway;
    }
    public void buildStairway(boolean stairway) {
        this.stairway = stairway;
    }

    public LevelNumber getLevelNumber() {
        return levelNumber;
    }
    public void setLevelNumber(LevelNumber levelNumber) {
        this.levelNumber = levelNumber;
    }
}
