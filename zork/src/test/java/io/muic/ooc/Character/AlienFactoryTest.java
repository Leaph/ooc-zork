package io.muic.ooc.Character;

import io.muic.ooc.Character.Alien;
import io.muic.ooc.Character.AlienFactory;
import org.junit.Test;

/**
 * Created by tyeon on 1/31/17.
 */
public class AlienFactoryTest {

    @Test
    public void createWeakAlien() throws Exception {
        AlienFactory alienFactory = new AlienFactory();
        Alien alien = alienFactory.createAlien("default");

        assert alien instanceof Alien && alien.getHP() == 100;
    }

    @Test
    public void createStrongAlien() throws Exception {
        AlienFactory alienFactory = new AlienFactory();
        Alien alien = alienFactory.createAlien("strong");

        assert alien instanceof Alien && alien.getHP() == 200;
    }

    @Test
    public void createBossAlien() throws Exception {
        AlienFactory alienFactory = new AlienFactory();
        Alien alien = alienFactory.createAlien("boss");

        assert alien instanceof Alien && alien.getHP() == 500;
    }

}