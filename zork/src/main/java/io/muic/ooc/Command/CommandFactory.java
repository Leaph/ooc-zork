package io.muic.ooc.Command;

import io.muic.ooc.Command.Commands.*;

import java.util.HashMap;

/**
 * Created by tyeon on 2/1/17.
 */
public class CommandFactory {

    private static HashMap<String, Class<? extends Command>> commandClasses =
            new HashMap<String, Class<? extends Command>>() {
                {
                    put("walk", WalkCommand.class);
                    put("quit", ExitCommand.class);
                    put("info", InfoCommand.class);
                    put("help", HelpCommand.class);
                    put("take", TakeCommand.class);
                    put("drop", DropCommand.class);
                    put("use", UseCommand.class);
                    put("fight", FightCommand.class);
                    put("climb", ClimbCommand.class);
                }
            };

    public Command getCommand(String[] commandLine) {
        String cmd = (commandLine.length > 0) ? commandLine[0] : "";
        String arg = (commandLine.length > 1) ? commandLine[1] : "";

        Class<? extends Command> commandClass = commandClasses.get(cmd);
        Command command = null;
        try {
            command = commandClass.newInstance();
            command.setArg(arg);
        } catch (InstantiationException | IllegalAccessException ex) {
        } catch (NullPointerException npe) {
            System.out.print("Unknown command [" + cmd + "]. Available commands: ");
            for (HashMap.Entry<String, Class<? extends Command>> c : commandClasses.entrySet()) System.out.print(c.getKey()+" ");
            System.out.println();
        }
        return command;
    }
}
