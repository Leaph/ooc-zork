package io.muic.ooc.Item.Armoury;

import io.muic.ooc.Item.Item;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by tyeon on 2/3/17.
 */
public class WeaponTest {

    @Test
    public void weapon() throws Exception {
        Weapon weapon = new Weapon();
        String name = "weapon";
        weapon.setName(name);
        assert weapon instanceof Item && weapon instanceof Weapon && weapon.getName().equals(name);
    }

    @Test
    public void getAndSetDurability() throws Exception {
        Weapon weapon = new Weapon();
        weapon.setDurability(50);
        assert weapon.getDurability() == 50;
    }

    @Test
    public void getAndSetMultiplier() throws Exception {
        Weapon weapon = new Weapon();
        weapon.setMultiplier(0.5);
        assert weapon.getMultiplier() == 0.5;
    }

}